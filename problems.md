Git merges most texty things fine, all by itself, no questions asked, as long as it has enough markers to figure it out. This *can* still cause problems if two editors add the same idea in different ways.

---

Direct conflicts in text files are marked out in the file so you can resolve them. Open each conflicted text file in an editor, choose or create the winning text, remove the markers from the file, and `add` the resulting good file to the index.

NOTE: Your final file must be clean of markers. It must be the file as you mean to store it for this version. git performs no magic.

---

Binary file conflicts are resolved whole-file by choosing the winner or creating a winner of the same name and `add`ing it.

With a conflicted binary blob named bob, for example:

To choose 'ours', the version from the *current* branch:

```shell
git checkout --ours bob
```

To choose 'theirs', the version from the branch being merged:

```shell
git checkout --theirs bob
```

To create a new winner, you might want to open ours and theirs and create a new bob for this merge in whatever application makes binaries of the type bob is.

However you arrive with the final merged version of bob, you must `add` it to resolve the conflict.

```shell
git add bob
```

and if you've resolved all the conflicts, you can finish the merge with:

```shell
git commit
```

---

Two ways to remove a bad change from the stage before committing:

- `git add (fixed_file)` to overwrite it
- `git reset (broken_file)` to remove it

`reset` removes the file from the stage; it doesn't change the live file or the last-committed version.

---

To see the history of the git repository, try `git log` or git-log-a-dog.

![gitlogadog](gitlogadog.jpg)

I prefer to add color, so I guess mine is git-log-a-color-dog.

`git log --all --color --decorate --oneline --graph`

Also try `git log -n 3` and other combinations of the individual log switches.

---

To checkout an older version of the repository, specify it by (part of) its hash.

For example, if the log shows that the hash of last Thursday's commit was e44ee1a889503f1b7f57e4eac5f58b25756f35b3, you can check it out with `git checkout e44ee1a889503f1b7f57e4eac5f58b25756f35b3` or enough of the hash to uniquely identify it. In a repository with only a few hundred or thousand commits, four characters is probably enough: `git checkout e44e`

To return to the most present, just `git checkout master`.
