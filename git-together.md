Git Together

## How to collaborate

With documentation: Share early and often. Don't leave your team in the dark. Don't be a hoarder. Don't aggravate the knowledge silo problem. Let people know what you know and let them contribute what they know.

If you reference another file, include a link, not just words.

With code, too: Share early and often. Don't let a large amount change build up. Don't put your work or your project, at greater risk than necessary. Minimize the error surface between tests. As soon as it works on your machine, get it in for integration tests!

Frequent, small changes are good. Big changes are bad. Old changes are bad. It is easier to find a bug in a smaller change.

## Sharing a git repository
git clone (address of shared repo)

Make some changes by adding or changing lines.

git pull ; git commit ; git push

Do this often, to keep your merge surface as small as possible and to make your changes in a timely fashion.

## GitHub Pull Request / GitLab Merge Request

When you don't have direct write access to a project, you'll submit a PR or MR. What if you need something fixed and you know just how to fix it? How should you make your suggestion?

fork, clone, edit, commit, push, submit a pull/merge request

## Exercise

- git remote add && git push
- Add students to this project
- Have one or more of them make changes
- Make concurrent local change
- Show how git auto-merges on pull

## Pair and share

- Students add each other to their GL projects
