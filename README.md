# Git and Markdown

By the end of this class, you should understand how and when to use the following features.

With [git](https://git-scm.org) and/or [GitLab](www.gitlab.com):
1. clone, fork, and init
- add and 'ignore'
- commit
- pull and push
- remote
- status
- branch

With [Markdown](https://daringfireball.net/projects/markdown/):
1. Headers
- Ordered, un-ordered, and hierarchical lists
- Links
- In-line graphics
- Paragraphs and h-lines
- em/italics and strong/bold
- mono-space, quote, and code-blocks

---

[GitLab and git](git-tools.md)

[Markdown](markdown.md)

[Git Alone](git-alone.md)

[Git Remote](git-remote.md)

[Git Together](git-together.md)

[Problems](problems.md)

---

## Notes:

I tend to use the words 'stage' and 'index' interchangeably. Forgive me.


## Do you want to know more?

Check out branching strategies and merge requests.


### If time allows
- Install Atom
- Write a git extension: `git-extension` also runs as `git extension`. so cool.
- Check /etc/ into git, make a system change, and use `git diff` to discover the config effects.
- Create a GitLab Issue with a check-list
- GitLab fork and merge
