Overview and Prerequisites
===

## What is git and why does it matter?

Machines need code and people need docs. Complex programs and procedures have bugs because people make mistakes. Complex programs and procedures need improvements because people learn.

Git, like every source-code manager (SCM), keeps revision history, so you can back up when you screw up, and you can see exactly who screwed up what, where, and when. Yes, it's a feature. You can also see who improved what, where, and when.

Git has branches, so you can do big things in an alternate time-line, until you're sufficiently sure you haven't killed your mother.

Git is small and fast.

Git makes merging changes from one repository to another, as easy as possible.

GitHub.com and GitLab.com are public sites where people publish projects.

GitHub Enterprise, GitLab Enterprise, cgit, and GitLab Community are softwares for sharing projects.

### Demo: Get on GitLab, clone this project, and create your own project

0. Ensure that you have git installed.
  * Linux:   `apt|yum|dnf install git`
  * Windows: https://git-scm.com/download/win
  * MacOSX:  https://git-scm.com/download/mac
-  Ensure that you have a [GitLab](https://gitlab.com/) account.
-  Ensure that you have an ssh keypair.
  * `ssh-keygen`
  * always use a good password on your personal private keys
  * good password = easy to remember and hard to guess
-  Ensure that your ssh public key is authorized in GitLab.
-  Clone this repository

   ```bash
   cd
   mkdir git
   cd git
   git clone git@gitlab.com:sofreeus/intro-to-git-and-markdown.git
   ```

### Pair and share:

As above, but skip the pieces you already have done.

Help each other.

If you have Windows or MacOS, you may need to download git from git-scm.org.
