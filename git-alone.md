Git (alone)
===

Git maintains a tree of commits and a staging area under a live file-system.

Run `git status` a lot. Files in the file-system that have been ignored or that match the most-recent commit are not mentioned by 'git status'.

Run `git diff` to see changes in files.

Run `git diff --cached` to see staged changes.

Run `git add .` to add all the files in the current directory to the stage.

Add garbage files to `.gitignore`

Run `git commit` whenever the stage has a complete change.

## Exercise:

FIXME: Break this into smaller parts.

Configure your git.

```bash
git config --global user.name "David L. Willson"
git config --global user.email "DLWillson@SoFree.US"
git config --global push.default simple
```


Start with some files.

```bash
git init
git add somefile
git status
```

- git commit
- git status
- change a file (add butter?)

```bash
git add cheese-sandwich.md
```

- git status
- change the file again (add habanero?)
- git status
- git commit -m 'update'
- create a garbage file
- ignore it by ls'ing it into .gitignore
- git status
- git add -A
- git commit -m 'ignore garbage'

Add your remote copy.

```bash
git remote add origin git@gitlab.com:fredzyp/git-and-markdown.git
git push -u origin master
```

Ignore some garbage files.
