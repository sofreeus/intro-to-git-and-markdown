## What is Markdown and why does it matter?

Markdown is a set of well-known text-formatting conventions that allow participants in a DevOps team (devs, ops, and others) to create documents that are usable as text and rich text (HTML). Because Markdown documents are text, tracking of changes is efficient for git and easy for you to review.

There are *lots* of other lightweight markup lanuages (pandoc, et al), but Markdown, for whatever reason, has been more widely adopted than any other set of conventions. Mattermost, Slack, Wordpress (with a plugin), and many other popular projects support it.

When team-members share procedures, other team-members can give input. When they share procedures and every has and uses git skills, they can give direct and unambiguous input.

  * we read for shared understanding
  * we write to co-create
  * we keep history because that facilitates due credit and corrections

---

#### Samples

- Headers === --- # ## ### and so on ...
- Hierarchical Lists:
  * Ordered
    1. first
    +  next
    +  last
  * Un-ordered
    + lions
    + tigers
    + bears
  * check-lists
    + [ ] make a plan
    + [ ] work the plan
    + [ ] retrospect
- **Bold** and *italics*
- `backticks make monotype`

Links
  * [git project](https://git-scm.com/)
  * [Software Freedom School](http://www.sofree.us/)

In-line images are like links but preceded by a ...

![bang](bang.png)

Paragraph breaks are *two* newlines,
not
just
one.

Quotes are preceded by '> '

> The true sign of intelligence is not knowledge but imagination.
> --Einstein

Pre-formatted (system) text is indented four spaces:

    $ fortune
    Talkers are no good doers.
    		-- William Shakespeare, "Henry VI"

Or bracketed in triple-backticks:

```
$ cowsay "I am enough of the artist to draw freely upon my imagination. Imagination is more important than knowledge. Knowledge is limited. Imagination encircles the world. --Einstein"
 _________________________________________
/ I am enough of the artist to draw       \
| freely upon my imagination. Imagination |
| is more important than knowledge.       |
| Knowledge is limited. Imagination       |
\ encircles the world. --Einstein         /
 -----------------------------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```

Or, to put `system text` in-line, use single back-ticks.

Highlighted code-blocks (GFM) are bracketed in triple-backticks and language specified.

```python
#!/usr/bin/env python
def fun():
    print("Markdown is fun")
if __name__ == "__main__":
    fun()
```


### Demo:

1. Write "These are a few of my favorite things"
-  Write "Write these are some parts of my favorite things"
-  Write "How to make a toasted cheese sandwich"

### Pair and share:

1. Same as above
